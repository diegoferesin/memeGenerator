$(function() {

  var imagenPrincipal = $('#meme-background-image');
  console.log('Esto es la imagen principal:', imagenPrincipal);
  
  var imagenes = $('#meme-options img');
  console.log('Estas son todas las imágenes:', imagenes);
  
  imagenes.click(function(){
    var nuevaImagen = $(this).attr('src');
    imagenPrincipal.attr('src', nuevaImagen);
  })
 
  var inputSuperior = $('#top-text');
  var inputInferior = $('#bottom-text');
  console.log('Esto es el input superior', inputSuperior);
  console.log('Esto es el input inferior', inputInferior);

  var tituloSuperior = $('#meme-text-top'); 
  var titutloInferior = $('#meme-text-bottom');
  console.log('Esto es el título superior', tituloSuperior);
  console.log('Esto es el título inferior', titutloInferior);
  
  inputSuperior.on('input', function(){
    var texto = inputSuperior.val();
    console.log(texto);
    tituloSuperior.text(texto);
  })

  inputInferior.on('input', function(){
    var texto = inputInferior.val();
    console.log(texto);
    titutloInferior.text(texto);
  })

});

